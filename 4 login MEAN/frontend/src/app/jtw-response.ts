export interface JtwResponseI {
    daraUser:{
        id: number,
        name: string,
        email: string,
        accesstoken: string,
        expiresIn: string
    }
}
